#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D shadowMap;

struct Light {
  vec3 Position;
  vec3 Color;
  
  float Linear;
  float Quadratic;
  float Radius;
  
  mat4 LightsMtx;
};

const int NR_LIGHTS = 1;
uniform Light lights[NR_LIGHTS];

uniform vec3 viewPos; //CamPosition
//uniform mat4 ViewToWorldMtx;

const float constantK = 0.1f;
/*
float readShadowMap(vec3 eyeDir)
{
	mat4 cameraViewToWorldMatrix = inverse(worldToCameraViewMatrix);
	mat4 cameraViewToProjectedLightSpace = lightViewToProjectionMatrix * worldToLightViewMatrix * cameraViewToWorldMatrix;
	vec4 projectedEyeDir = cameraViewToProjectedLightSpace * vec4(eyeDir,1);
	projectedEyeDir = projectedEyeDir/projectedEyeDir.w;

	vec2 textureCoordinates = projectedEyeDir.xy * vec2(0.5,0.5) + vec2(0.5,0.5);

	const float bias = 0.0001;
	float depthValue = texture2D( tShadowMap, textureCoordinates ) - bias;
	return projectedEyeDir.z * 0.5 + 0.5 < depthValue;
}
*/
/*
float ShadowCalculation(vec4 eyeDir)
{
  //mat4 cameraViewToWorldMatrix = inverse(worldToCameraViewMatrix); = ViewToWorldMtx
  mat4 cameraViewToProjectedLightSpace = 
  vec4 projCoords = eyeDir;

  // perform perspective divide
  projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
  // transform to [0,1] range
  //projCoords = projCoords * 0.5 + 0.5;
  // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
  float closestDepth = texture(shadowMap, projCoords.xy).r;
  // get depth of current fragment from light's perspective
  float currentDepth = projCoords.z;
  // check whether current frag pos is in shadow
  float shadow = currentDepth > closestDepth  ? 1.0 : 0.0;
  
  return shadow;
}
*/
void main()
{
  // retrieve data from gbuffer
  vec3 FragPos = texture(gPosition, TexCoords).rgb;
  vec3 Normal = texture(gNormal, TexCoords).rgb;
  vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
  float Specular = texture(gAlbedoSpec, TexCoords).a;
  
  // then calculate lighting as usual
  vec3 lighting  = Diffuse * 0.1; // hard-coded ambient component
  vec3 viewDir  = normalize(viewPos - FragPos);
  for(int i = 0; i < NR_LIGHTS; ++i)
  {
    float dist = length(lights[i].Position - FragPos);
    if(dist < lights[i].Radius)
    {
      // diffuse
      vec3 lightDir = normalize(lights[i].Position - FragPos);
      vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lights[i].Color;
      // specular
      vec3 halfwayDir = normalize(lightDir + viewDir);
      float spec = pow(max(dot(Normal, halfwayDir), 0.0), 16.0);
      vec3 specular = lights[i].Color * spec * Specular;
      // attenuation
      float distance = length(lights[i].Position - FragPos);
      float attenuation = 1.0 / (constantK + lights[i].Linear * distance + lights[i].Quadratic * distance * distance);
      diffuse *= attenuation;
      specular *= attenuation;
      // Shadow
      mat4 lightSpaceTrans = lights[i].LightsMtx;
      vec4 projCoords = lightSpaceTrans * vec4(FragPos,1.0);

      projCoords.xyz /= projCoords.w;
      projCoords.xyz = projCoords.xyz * 0.5 + 0.5;
      
      float closestDepth = texture(shadowMap, projCoords.xy).r;
      float currentDepth = projCoords.z;
      
      float bias = max(0.05 * (1.0 - dot(Normal, lightDir)), 0.005);

      float visibility = currentDepth - bias < closestDepth ? 1.0 : 0.0;
    
      // Result
      lighting += visibility * ( diffuse + specular );
      //lighting = vec3(visibility);
    }
  }
  FragColor = vec4(lighting, 1.0);
}
