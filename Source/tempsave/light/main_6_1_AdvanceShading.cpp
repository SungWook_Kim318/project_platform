// GL Libs
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Basic Libs
#include <iostream>

// Custom Libs
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "../extern/stb_image.h"

void windowSize_CB(GLFWwindow* window, int width, int height);
void Keyboard_CB(GLFWwindow *window);
void Mouse_CB(GLFWwindow* window, double xpos, double ypos);
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset);
unsigned int loadTexture(const char *path);

// settings
constexpr unsigned int winSizeWidth = 1280;
constexpr unsigned int winSizeHeight = 720;
bool blinn = false;
bool blinnKeyPressed = false;
// camera
Camera g_cam(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = winSizeWidth / 2.0;
float lastY = winSizeHeight / 2.0;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;  // time between current frame and last frame
float lastFrame = 0.0f;

// lighting
glm::vec3 lightPos(1.2f, 1.0f, 2.0f); 

int main(int argc, char** argv)
{
  //--------------------------- INIT PART ---------------------------
  bool res = glfwInit();
  if(!res)
  {
    std::cout << "ERROR! glfw init\n";
    return -1;
  }
  //macOS setting
  //OpenGL 4.1
#ifdef __APPLE__
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#else
  //Other setting
  //OpenGL 4.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
  
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  
  //Create Process and Window
  GLFWwindow* window = glfwCreateWindow(winSizeWidth, winSizeHeight, "LearnOpenGL", NULL, NULL);
  if (window == NULL)
  {
    //TODO : change throwing exception.
    //throw()
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  
  //Add Callback function to change Window Size
  //Make Make GL Context
  glfwMakeContextCurrent(window);
  
  // tell GLFW to capture our mouse
  glfwSetFramebufferSizeCallback(window, windowSize_CB);
  glfwSetCursorPosCallback(window, Mouse_CB);
  glfwSetScrollCallback(window, MouseScroll_CB);

  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  
  //Init glew
  glewExperimental = true; // Needed for core profile
  if( glewInit() != GLEW_OK )
  {
    fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
    getchar();
    glfwTerminate();
    return -1;
  }
  
  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  //glEnable(GL_CULL_FACE);
  // --------------------------- SHADER LOAD AND LINK PART ---------------------------
  Shader shader("1.advanced_lighting.vert", "1.advanced_lighting.frag");
  // --------------------------- Load Model ---------------------------
  // set up vertex data (and buffer(s)) and configure vertex attributes
  float planeVertices[] = {
    // positions            // normals         // texcoords
    10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
    -10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
    -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,
    
    10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
    -10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,
    10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,  10.0f, 10.0f
  };
  // plane VAO
  unsigned int planeVAO, planeVBO;
  glGenVertexArrays(1, &planeVAO);
  glGenBuffers(1, &planeVBO);
  glBindVertexArray(planeVAO);
  glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
  glBindVertexArray(0);

  // load textures (we now use a utility function to keep the code more organized)
  // -----------------------------------------------------------------------------
  unsigned int diffuseMap = loadTexture("container2.png");
  unsigned int specularMap = loadTexture("container2_specular.png");
  // shader configuration
  // --------------------
  unsigned int floorTexture = loadTexture("wood.png");

  shader.use();
  shader.setInt("texture1", 0);
  
  // lighting info
  // -------------
  glm::vec3 lightPos(0.0f, 0.0f, 0.0f);
  
  // --------------------------- UPDATE PART ---------------------------
  //TODO: Game Update
  do {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    // --------- handling input
    Keyboard_CB(window);
    
    // --------- render
    //Clear BackGround
    //glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // draw objects
    shader.use();
    glm::mat4 projection{
      glm::perspective(glm::radians(g_cam.Zoom),
                       (float)winSizeWidth / (float)winSizeHeight,
                       0.1f, 100.0f )
    };
    glm::mat4 view{ g_cam.GetViewMatrix() };
    shader.setMat4("projection", projection);
    shader.setMat4("view", view);
    // set light uniforms
    shader.setVec3("viewPos", g_cam.Position);
    shader.setVec3("lightPos", lightPos);
    shader.setInt("blinn", blinn);
    // floor
    glBindVertexArray(planeVAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, floorTexture);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    
    std::cout << (blinn ? "Blinn-Phong" : "Phong") << std::endl;

    // -------- glfw Update
    glfwSwapBuffers(window);
    glfwPollEvents();
    
  } while (!glfwWindowShouldClose(window));
  
  // --------------------------- END PART ---------------------------
  //TODO: Game Close
  //Delete GLFW window and data
  glDeleteVertexArrays(1, &planeVAO);
  glDeleteBuffers(1, &planeVBO);

  // glfw: terminate, clearing all previously allocated GLFW resources.
  // ------------------------------------------------------------------
  glfwTerminate();
  
  return 0;
}
// Callback Function
void windowSize_CB(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}
//TODO: Move Input manager.
void Keyboard_CB(GLFWwindow *window)
{
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    g_cam.ProcessKeyboard(FORWARD, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    g_cam.ProcessKeyboard(BACKWARD, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    g_cam.ProcessKeyboard(LEFT, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    g_cam.ProcessKeyboard(RIGHT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS && !blinnKeyPressed)
  {
    blinn = !blinn;
    blinnKeyPressed = true;
  }
  if (glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE)
  {
    blinnKeyPressed = false;
  }
}
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void Mouse_CB(GLFWwindow* window, double xpos, double ypos)
{
  if(firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

  lastX = xpos;
  lastY = ypos;

  g_cam.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  g_cam.ProcessMouseScroll(yoffset);
}


// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture(char const * path)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);

  int width, height, nrComponents;
  unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
  if(data)
  {
    GLenum format;
    if(nrComponents == 1)
      format = GL_RED;
    else if(nrComponents == 3)
      format = GL_RGB;
    else if(nrComponents == 4)
      format = GL_RGBA;
    else
    {}
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_image_free(data);
  }
  else
  {
    std::cout << "Texture failed to load at path: " << path << std::endl;
    stbi_image_free(data);
  }

  return textureID;
}
