// GL Libs
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Basic Libs
#include <iostream>

// Custom Libs
#include "Shader.hpp"
#include "Model.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "../extern/stb_image.h"

void windowSize_CB(GLFWwindow* window, int width, int height);
void HandleInput_CB(GLFWwindow *window);
void Mouse_CB(GLFWwindow* window, double xpos, double ypos);
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset);

//Cam
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

bool firstMouse = true;
float yaw   = -90.0f;  // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch =  0.0f;
float lastX =  800.0f / 2.0;
float lastY =  600.0 / 2.0;
float fov   =  45.0f;

// timing
float deltaTime = 0.0f;  // time between current frame and last frame
float lastFrame = 0.0f;

int main(int argc, char** argv)
{
  //--------------------------- INIT PART ---------------------------
  bool res = glfwInit();
  if(!res)
  {
    std::cout << "ERROR! glfw init\n";
    return -1;
  }
  //macOS setting
  //OpenGL 4.1
#ifdef __APPLE__
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#else
  //Other setting
  //OpenGL 4.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
  
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  
  //Create Process and Window
  
  constexpr int winSizeWidth  = 800;
  constexpr int winSizeHeight = 600;
  GLFWwindow* window = glfwCreateWindow(winSizeWidth, winSizeHeight, "LearnOpenGL", NULL, NULL);
  if (window == NULL)
  {
    //TODO : change throwing exception.
    //throw()
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  
  //Add Callback function to change Window Size
  //Make Make GL Context
  glfwMakeContextCurrent(window);
  
  // tell GLFW to capture our mouse
  glfwSetFramebufferSizeCallback(window, windowSize_CB);
  glfwSetCursorPosCallback(window, Mouse_CB);
  glfwSetScrollCallback(window, MouseScroll_CB);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  
  //Init glew
  glewExperimental = true; // Needed for core profile
  if( glewInit() != GLEW_OK )
  {
    fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
    getchar();
    glfwTerminate();
    return -1;
  }
  
  
  
  // Test depth
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_CULL_FACE);
  // --------------------------- SHADER LOAD AND LINK PART ---------------------------
  Shader myshrd("basic.vert","basic.frag");
  // --------------------------- Load Model ---------------------------
  Model ourModel("nanosuit/nanosuit.obj");
  // --------------------------- UPDATE PART ---------------------------
  //TODO: Game Update
  do {
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    // --------- handling input
    HandleInput_CB(window);
    
    // --------- render
    //Clear BackGround
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Used Shader
    myshrd.use();
    
    //glm::perspective(glm::radians(fov), static_cast<float>(winSizeWidth) / winSizeHeight, 0.1f, 100.f);
    
    // view/projection transformations
    glm::mat4 projection = glm::perspective(glm::radians(fov),
                                            static_cast<float>(winSizeWidth) / winSizeHeight,
                                            0.1f, 100.f);
    glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    myshrd.setMat4("projection", projection);
    myshrd.setMat4("view", view);
    
    // render the loaded model
    glm::mat4 model{1.0f};
    model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));  // it's a bit too big for our scene, so scale it down
    myshrd.setMat4("model", model);
    ourModel.Draw(myshrd);

    // -------- glfw Update
    glfwSwapBuffers(window);
    glfwPollEvents();
    
  } while (!glfwWindowShouldClose(window));
  
  // --------------------------- END PART ---------------------------
  //TODO: Game Close
  //Delete GLFW window and data
  glfwTerminate();
  
  return 0;
}
// Callback Function
void windowSize_CB(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}
//TODO: Move Input manager.
void HandleInput_CB(GLFWwindow *window)
{
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  float cameraSpeed = 2.5f * deltaTime; // adjust accordingly
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    cameraPos += cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    cameraPos -= cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void Mouse_CB(GLFWwindow* window, double xpos, double ypos)
{
  if (firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }
  
  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
  lastX = xpos;
  lastY = ypos;
  
  float sensitivity = 0.1f; // change this value to your liking
  xoffset *= sensitivity;
  yoffset *= sensitivity;
  
  yaw += xoffset;
  pitch += yoffset;
  
  // make sure that when pitch is out of bounds, screen doesn't get flipped
  if (pitch > 89.0f)
    pitch = 89.0f;
  if (pitch < -89.0f)
    pitch = -89.0f;
  
  glm::vec3 front;
  front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
  front.y = sin(glm::radians(pitch));
  front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
  cameraFront = glm::normalize(front);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  if (fov >= 1.0f && fov <= 45.0f)
    fov -= yoffset;
  if (fov <= 1.0f)
    fov = 1.0f;
  if (fov >= 45.0f)
    fov = 45.0f;
}
