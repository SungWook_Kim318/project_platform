// GL Libs
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Basic Libs
#include <iostream>
#include <limits>

// Custom Libs
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "../extern/stb_image.h"

void windowSize_CB(GLFWwindow* window, int width, int height);
void Keyboard_CB(GLFWwindow *window);
void Mouse_CB(GLFWwindow* window, double xpos, double ypos);
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset);
unsigned loadTexture(const char *path, bool gammaCorrection);
void renderCube();
void renderQuad();
void renderFloor(const unsigned diffuse, const int specular = -1);
// settings
unsigned int SCR_WIDTH = 1280;
unsigned int SCR_HEIGHT = 720;

// camera
Camera camera(glm::vec3(0.0f, 10.0f, 15.0f));
float lastX = SCR_WIDTH / 2.0;
float lastY = SCR_HEIGHT / 2.0;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;  // time between current frame and last frame
float lastFrame = 0.0f;

// Light Variable
static float linear = 0.5;
static float quadratic = 0.5;

int main(int argc, char** argv)
{
  //--------------------------- INIT PART ---------------------------
  bool res = glfwInit();
  if(!res)
  {
    std::cout << "ERROR! glfw init\n";
    return -1;
  }
  //macOS setting
  //OpenGL 4.1
  //glfwWindowHint(GLFW_SAMPLES, 8);

#ifdef __APPLE__
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#else
  //Other setting
  //OpenGL 4.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif
  
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  
  
  //Create Process and Window
  GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
  if (window == NULL)
  {
    //TODO : change throwing exception.
    //throw()
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  
  //Add Callback function to change Window Size
  //Make Make GL Context
  glfwMakeContextCurrent(window);
  
  // tell GLFW to capture our mouse
  glfwSetFramebufferSizeCallback(window, windowSize_CB);
  glfwSetCursorPosCallback(window, Mouse_CB);
  glfwSetScrollCallback(window, MouseScroll_CB);

  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  
  //Init glew
  glewExperimental = true; // Needed for core profile
  if( glewInit() != GLEW_OK )
  {
    fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
    getchar();
    glfwTerminate();
    return -1;
  }
  
  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);
  
  // Shaders
  Shader shaderGeometryPass{"8.2.g_buffer.vs","8.2.g_buffer.fs"};
  Shader shaderLightingPass{"8.2.deferred_shading.vs","8.2.deferred_shading.fs"};
  Shader shaderLightBox{"8.2.deferred_light_box.vs","8.2.deferred_light_box.fs"};
  
  Shader DepthShader{"depthBuffer.vs","depthBuffer.fs"};
  Shader DepthCubeShader{"depthCubeBuffer.vs","depthCubeBuffer.fs","depthCubeBuffer.gs"};
  
  Shader debugDepthQuad{"debug/debug_depth.vs","debug/debug_depth.fs"};
  Shader debugBufferQuad{"debug/debug_gbuffer.vs", "debug/debug_gbuffer.fs"};
  
  // Load model
  Model nanosuit{"nanosuit/nanosuit.obj"};
  std::vector<glm::vec3> objectPositions{};
  objectPositions.push_back(glm::vec3(-3.0,  -3.0, -3.0));
  objectPositions.push_back(glm::vec3( 0.0,  -3.0, -3.0));
  objectPositions.push_back(glm::vec3( 3.0,  -3.0, -3.0));
  objectPositions.push_back(glm::vec3(-3.0,  -3.0,  0.0));
  objectPositions.push_back(glm::vec3( 0.0,  -3.0,  0.0));
  objectPositions.push_back(glm::vec3( 3.0,  -3.0,  0.0));
  objectPositions.push_back(glm::vec3(-3.0,  -3.0,  3.0));
  objectPositions.push_back(glm::vec3( 0.0,  -3.0,  3.0));
  objectPositions.push_back(glm::vec3( 3.0,  -3.0,  3.0));
  //load texture of floor 
  unsigned int floorDiffuse = loadTexture("container2.png", true);
  unsigned int floorSpecular = loadTexture("container2_specular.png", true);
  // configure g-buffer framebuffer
  // ------------------------------
  unsigned int gBuffer;
  glGenFramebuffers(1, &gBuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
  unsigned int gPosition, gNormal, gAlbedoSpec;
  // position color buffer
  glGenTextures(1, &gPosition);
  glBindTexture(GL_TEXTURE_2D, gPosition);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);
  // normal color buffer
  glGenTextures(1, &gNormal);
  glBindTexture(GL_TEXTURE_2D, gNormal);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);
  // color + specular color buffer
  glGenTextures(1, &gAlbedoSpec);
  glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedoSpec, 0);
  // tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
  unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
  glDrawBuffers(3, attachments);
  // create and attach depth buffer (renderbuffer)
  unsigned int rboDepth;
  glGenRenderbuffers(1, &rboDepth);
  glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCR_WIDTH, SCR_HEIGHT);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
  // finally check if framebuffer is complete
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "Framebuffer not complete!" << std::endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  
  // lighting info
  // -------------
  glm::vec3 lightPositions;
  glm::vec3 lightColors;
  glm::vec3 lightVelo;
  constexpr float lightConstant = 0.1f;
  srand(13);
  
  lightPositions = glm::vec3{1.0,1.0,1.0};
  lightVelo = glm::vec3{0};
  lightColors = glm::vec3{0.8,1.0,1.0};
  
  // shader configuration
  // --------------------
  shaderLightingPass.use();
  shaderLightingPass.setInt("gPosition", 0);
  shaderLightingPass.setInt("gNormal", 1);
  shaderLightingPass.setInt("gAlbedoSpec", 2);
  shaderLightingPass.setInt("depthMap", 3);
  
  // Shadow Part - Make Depth FBO
  constexpr unsigned Depth_Width = 1024, Depth_Height = 1024;
  unsigned int depthMapFBO;
  glGenFramebuffers(1, &depthMapFBO);
  //create depth cube map texture
  unsigned depthCubeMap;
  glGenTextures(1, &depthCubeMap);
  glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
  for (unsigned int i = 0; i < 6; ++i)
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, Depth_Width, Depth_Height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  // attach depth texture as FBO's depth buffer
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMap, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "Framebuffer not complete!" << std::endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  
  
  // DEBUG
  debugDepthQuad.use();
  debugDepthQuad.setInt("depthMap", 0);
  
  // --------------------------- UPDATE PART ---------------------------
  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
  //TODO: Game Update
  do {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    // --------- handling input
    Keyboard_CB(window);
    
    // Physics Part
    // ----------------------------------------------------------------------
    /*
     
    */
    // ----------------------------------------------------------------------
    
    // render
    // ------
    // Clear buffers
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // 1. geometry pass: render scene's geometry/color data into gbuffer
    // -----------------------------------------------------------------
    
    // Start Draw Models
    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    glm::mat4 view = camera.GetViewMatrix();
    glm::mat4 model{1.0f};
    shaderGeometryPass.use();
    shaderGeometryPass.setMat4("projection", projection);
    shaderGeometryPass.setMat4("view", view);
  
    // Set Depth Map
    // -------------
    // Calculation lights view & projection mtx
    //glm::mat4 lightsMatrix{1.0};
    float near_plane = 1.0f;
    float far_plane = 25.0f;
    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), 
                                            static_cast<float>(Depth_Width) / static_cast<float>(Depth_Height), 
                                            near_plane, far_plane);
    std::vector<glm::mat4> shadowTransforms;
    shadowTransforms.reserve(6);
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPositions, lightPositions + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
    
    
    DepthCubeShader.use();
    for(unsigned i = 0 ; i < 6 ; ++i)
      DepthCubeShader.setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    DepthCubeShader.setFloat("far_plane", far_plane);
    DepthCubeShader.setVec3("lightPos", lightPositions);
    //glViewport(0, 0, Depth_Width, Depth_Height);
    //glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    //rendersence(DepthCubeShader);
    //glBindFrambuffer(GL_Frambuffer,0);
    for (unsigned int i = 0; i < objectPositions.size(); i++)
    {
      model = glm::mat4{1.0};
      model = glm::translate(model, objectPositions[i]);
      model = glm::scale(model, glm::vec3(0.25f));
      //shaderGeometryPass.use();
      //shaderGeometryPass.setMat4("model", model);
      //DepthCubeShader.use();
      //DepthCubeShader.setMat4("model", model);
      // Draw
      // ----
      // Draw on Depth buffer
      DepthCubeShader.use();
      glViewport(0, 0, Depth_Width, Depth_Height);
      DepthCubeShader.setMat4("model", model);
      glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
      nanosuit.Draw(DepthCubeShader);

      // Draw on G-Buffer
      shaderGeometryPass.use();
      glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
      shaderGeometryPass.setMat4("model", model);
      glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
      nanosuit.Draw(shaderGeometryPass);
      
      // End Draw = Reset target buffer to draw
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    // Draw Floor
    // ----------
    model = glm::mat4{1.0f};
    //Scale
    model = glm::scale(model, glm::vec3{6.f, 6.f, 6.f});
    //Rota. - Nope
    //model = glm::rotate(model, glm::pi<float>() / 2.f, glm::vec3{1.0f, 1.0f, 1.0f});
    //Tras.
    model = glm::translate(model, glm::vec3{0.f, -0.5f, 0.f});
    //Pass TRS matrix
    shaderGeometryPass.use();
    shaderGeometryPass.setMat4("model", model);
    DepthCubeShader.use();
    DepthCubeShader.setMat4("model", model);
    
    // Draw
    // ----
    // Draw on Depth buffer
    DepthShader.use();
    glViewport(0, 0, Depth_Width, Depth_Height);
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    renderFloor(floorDiffuse, floorSpecular);

    // Draw on G-buffer
    shaderGeometryPass.use();
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
    renderFloor(floorDiffuse, floorSpecular);
    // End draw, Reset target buffer to draw
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    // 2. lighting pass: calculate lighting by iterating over a screen filled quad pixel-by-pixel using the gbuffer's content.
    // -----------------------------------------------------------------------------------------------------------------------
    // shader -> Draw lighting shader
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shaderLightingPass.use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
    // set lighting uniforms
    shaderLightingPass.setVec3("lightPos", lightPositions);
    shaderLightingPass.setVec3("lightColor", lightColors);
    shaderLightingPass.setFloat("lightConstant", lightConstant);
    shaderLightingPass.setFloat("lightLinear", linear);
    shaderLightingPass.setFloat("lightQuadratic", quadratic);
    shaderLightingPass.setInt("shadows", 1); // enable/disable shadows by pressing 'SPACE'
    shaderLightingPass.setFloat("far_plane", far_plane);
    shaderLightingPass.setVec3("viewPos", camera.Position);
    
    
    //shaderLightingPass.setMat4("ViewToWorldMtx", glm::inverse(projection*view));
    // finally render quad
    renderQuad();
    
    // 2.5. copy content of geometry's depth buffer to default framebuffer's depth buffer
    // ----------------------------------------------------------------------------------
    glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // write to default framebuffer
    // blit to default framebuffer. Note that this may or may not work as the internal formats of both the FBO and default framebuffer have to match.
    // the internal formats are implementation defined. This works on all of my systems, but if it doesn't on yours you'll likely have to write to the
    // depth buffer in another shader stage (or somehow see to match the default framebuffer's internal format with the FBO's internal format).
    glBlitFramebuffer(0, 0, SCR_WIDTH, SCR_HEIGHT, 0, 0, SCR_WIDTH, SCR_HEIGHT, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    // 3. render lights on top of scene
    // --------------------------------
    shaderLightBox.use();
    shaderLightBox.setMat4("projection", projection);
    shaderLightBox.setMat4("view", view);
    model = glm::mat4{1.0};
    model = glm::translate(model, lightPositions);
    model = glm::scale(model, glm::vec3(0.125f));
    shaderLightBox.setMat4("model", model);
    shaderLightBox.setVec3("lightColor", lightColors);
    renderCube();
    // render Depth map to quad for visual debugging
    // ---------------------------------------------
    debugDepthQuad.use();
    debugDepthQuad.setFloat("near_plane", near_plane);
    debugDepthQuad.setFloat("far_plane", far_plane);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
    glViewport(0, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    // Render G buffer map to quad for visual debugginf
    debugBufferQuad.use();
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glViewport(SCR_WIDTH / 8, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glViewport(SCR_WIDTH / 8*2, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
    glViewport(SCR_WIDTH / 8*3, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    // -------- glfw Update
    glfwSwapBuffers(window);
    glfwPollEvents();
    
  } while (!glfwWindowShouldClose(window));
  
  // ---------------------------- END PART ----------------------------
  // glfw: terminate, clearing all previously allocated GLFW resources.
  // ------------------------------------------------------------------
  glfwTerminate();
  
  return 0;
}

// renderCube() renders a 1x1 3D cube in NDC.
// -------------------------------------------------
unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;
void renderCube()
{
  // initialize (if necessary)
  if (cubeVAO == 0)
  {
    float vertices[] = {
      // back face
      -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
      1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
      1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right
      1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
      -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
      -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
      // front face
      -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
      1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
      1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
      1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
      -1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
      -1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
      // left face
      -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
      -1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
      -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
      -1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
      -1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
      -1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
      // right face
      1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
      1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
      1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right
      1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
      1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
      1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left
      // bottom face
      -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
      1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
      1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
      1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
      -1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
      -1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
      // top face
      -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
      1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
      1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right
      1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
      -1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
      -1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left
    };
    glGenVertexArrays(1, &cubeVAO);
    glGenBuffers(1, &cubeVBO);
    // fill buffer
    glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // link vertex attributes
    glBindVertexArray(cubeVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }
  // render Cube
  glBindVertexArray(cubeVAO);
  glDrawArrays(GL_TRIANGLES, 0, 36);
  glBindVertexArray(0);
}
// renderFloor is made ref. quad function
unsigned FloorVAO = 0;
unsigned FloorVBO;
void renderFloor(const unsigned diffuse, const int specular)
{
  if(FloorVAO == 0)
  {
    float floorVertices[] =
    {
      // positions          // normals         // texcoords
       1.0f, 0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  2.0f, 0.0f,
      -1.0f, 0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 0.0f,
      -1.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 2.0f,
      
       1.0f, 0.0f,  1.0f,  0.0f, 1.0f, 0.0f,  2.0f, 0.0f,
      -1.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  0.0f, 2.0f,
       1.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f,  2.0f, 2.0f
    };
    // setup plane VAO
    glGenVertexArrays(1, &FloorVAO);
    glGenBuffers(1, &FloorVBO);
    glBindVertexArray(FloorVAO);
    glBindBuffer(GL_ARRAY_BUFFER, FloorVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(floorVertices), &floorVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(0);
  }
  glBindVertexArray(FloorVAO);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, diffuse);
  if(specular != -1)
  {
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specular);
  }
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0);
}
// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
  if (quadVAO == 0)
  {
    float quadVertices[] = {
      // positions        // texture Coords
      -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
      -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
      1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
      1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
    };
    // setup plane VAO
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  }
  glBindVertexArray(quadVAO);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);
}

// Callback Function
void windowSize_CB(GLFWwindow* window, int width, int height)
{
  SCR_WIDTH = width;
  SCR_HEIGHT = height;
  glViewport(0, 0, width, height);
}
//TODO: Move Input manager.
void Keyboard_CB(GLFWwindow *window)
{
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.ProcessKeyboard(FORWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.ProcessKeyboard(BACKWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.ProcessKeyboard(LEFT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.ProcessKeyboard(RIGHT, deltaTime);
  
  //change Light Power
  if(glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
    linear += 0.01f;
  else if(glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
    linear <= 0.0001f ? linear = 0.0001f : linear -= 0.01f;
  
  if(glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
    quadratic += 0.01f;
  else if(glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
    quadratic <= 0.0001f ? quadratic = 0.0001f : quadratic -= 0.01f;
}
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void Mouse_CB(GLFWwindow* window, double xpos, double ypos)
{
  if(firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

  lastX = xpos;
  lastY = ypos;

  camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  camera.ProcessMouseScroll(yoffset);
}


// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned loadTexture(const char *path, bool gammaCorrection)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  
  int width, height, nrComponents;
  unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
  if (data)
  {
    GLenum internalFormat = 0;
    GLenum dataFormat = 0;
    if (nrComponents == 1)
    {
      internalFormat = dataFormat = GL_RED;
    }
    else if (nrComponents == 3)
    {
      internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
      dataFormat = GL_RGB;
    }
    else if (nrComponents == 4)
    {
      internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
      dataFormat = GL_RGBA;
    }
    
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    stbi_image_free(data);
  }
  else
  {
    std::cout << "Texture failed to load at path: " << path << std::endl;
    stbi_image_free(data);
  }
  
  return textureID;
}
