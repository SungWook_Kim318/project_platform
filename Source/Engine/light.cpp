//
//  light.cpp
//  playground
//
//  Created by kmu-lab on 17/10/2018.
//

#include "light.hpp"

#include "Shader.hpp"
#include <glm/trigonometric.hpp>
#include <GL/glew.h>
#include "Camera.hpp"
#include "Graphics.hpp"


unsigned DeferredPointShadow::s_textureId = 0;
unsigned DeferredPointShadow::s_shaderId = 0;
unsigned DeferredPointShadow::s_depthShaderID = 0;
gBuffer DeferredPointShadow::gbuf;

std::array<unsigned, 2> DeferredPointShadow::s_framebuffer = {};
std::array<unsigned, 2> DeferredPointShadow::s_textureColorBuffer{};
bool DeferredPointShadow::s_num = 0;

extern unsigned int SCR_WIDTH;
extern unsigned int SCR_HEIGHT;
extern Camera camera;

DeferredPointShadow::DeferredPointShadow(const DeferredPointShadow& o)
: m_pos(o.m_pos), m_color(o.m_color), m_power(o.m_power),
m_rad(o.m_rad), m_near(o.m_near), m_far(o.m_far), m_shadowMtx(o.m_shadowMtx), m_mapSize(o.m_mapSize), m_depthMapFBO(o.m_depthMapFBO), m_depthCubeMap(o.m_depthMapFBO), initailed(o.initailed)
{}

DeferredPointShadow::~DeferredPointShadow(void)
{ 
  /*
  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &skyboxVAO);
  glDeleteBuffers(1, &cubeVBO);
  glDeleteBuffers(1, &skyboxVAO);
  */
}

bool DeferredPointShadow::init(void)
{
  glGenFramebuffers(1, &m_depthMapFBO);

  glGenTextures(1, &m_depthCubeMap);
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_depthCubeMap);
  for (unsigned int i = 0; i < 6; ++i)
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, m_mapSize.x, m_mapSize.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  
  glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_depthCubeMap, 0);
  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);
  
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    return initailed = false;
  
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  return initailed = true;
}
bool DeferredPointShadow::update(void)
{
  if(!initailed)
    return false; // or throw();

  glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
  glClear(GL_DEPTH_BUFFER_BIT);

  glm::mat4 shadowProj = glm::perspective(m_rad,
                                          static_cast<float>(m_mapSize.x) / static_cast<float>(m_mapSize.y),
                                          m_near, m_far);

  //const glm::vec3& pos{m_pos};
  constexpr glm::vec3 upX  { 1.0f,  0.0f,  0.0f};
  constexpr glm::vec3 downX{-1.0f,  0.0f,  0.0f};
  constexpr glm::vec3 upY  { 0.0f,  1.0f,  0.0f};
  constexpr glm::vec3 downY{ 0.0f, -1.0f,  0.0f};
  constexpr glm::vec3 upZ  { 0.0f,  0.0f,  1.0f};
  constexpr glm::vec3 downZ{ 0.0f,  0.0f, -1.0f};

  m_shadowMtx[0] = shadowProj * glm::lookAt(m_pos, m_pos + upX, downY);
  m_shadowMtx[1] = shadowProj * glm::lookAt(m_pos, m_pos + downX, downY);
  m_shadowMtx[2] = shadowProj * glm::lookAt(m_pos, m_pos + upY, upZ);
  m_shadowMtx[3] = shadowProj * glm::lookAt(m_pos, m_pos + downY, downZ);
  m_shadowMtx[4] = shadowProj * glm::lookAt(m_pos, m_pos + upZ, downY);
  m_shadowMtx[5] = shadowProj * glm::lookAt(m_pos, m_pos + downZ, downY);

  return true;
}




void DeferredPointShadow::DrawDepthMap(Model& object, const glm::mat4& MVP)
{ 
  Shader DepthCubeShader{s_depthShaderID};
  //TODO: move to Draw function

  DepthCubeShader.use();
  for(unsigned i = 0 ; i < 6 ; ++i)
    DepthCubeShader.setMat4("shadowMatrices[" + std::to_string(i) + "]", m_shadowMtx[i]);
  DepthCubeShader.setFloat("far_plane", m_far);
  DepthCubeShader.setVec3("lightPos", m_pos);

  DepthCubeShader.setMat4("model", MVP);

  glViewport(0, 0, m_mapSize.x, m_mapSize.y);
  glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
  object.Draw(DepthCubeShader);

  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredPointShadow::Draw(void)
{
  /*
   num  - 1 - 0 -
   Frnt - 0 - 1 -
   Back - 1 - 0 -
  */
  glBindFramebuffer(GL_FRAMEBUFFER, s_framebuffer[static_cast<int>(s_num)]);
  GLenum error;

  glClearColor(0, 0, 0, 0);
  Shader shaderLightingPass{s_shaderId};
  
  shaderLightingPass.use();
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, gbuf.gWorldPos);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, gbuf.gNormal);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, gbuf.gAlbedo);
  glActiveTexture(GL_TEXTURE3);
  error = glGetError();
  if(error != GL_NO_ERROR)
    throw("");
  glBindTexture(GL_TEXTURE_CUBE_MAP, m_depthCubeMap);
  error = glGetError();
  if(error != GL_NO_ERROR)
    throw("");
  glActiveTexture(GL_TEXTURE4);
  s_num = !s_num;
  glBindTexture(GL_TEXTURE_2D, s_textureColorBuffer[static_cast<int>(s_num)]);
  error = glGetError();
  if(error != GL_NO_ERROR)
    throw("");
  // set lighting uniforms
  shaderLightingPass.setVec3("lightPos", m_pos);
  shaderLightingPass.setVec3("lightColor", m_color);
  shaderLightingPass.setFloat("lightConstant", m_power.diffuse.r);
  shaderLightingPass.setFloat("lightLinear", m_power.linear.r);
  shaderLightingPass.setFloat("lightQuadratic", m_power.quadratic.r);
  shaderLightingPass.setInt("shadows", 1); // enable/disable shadows by pressing 'SPACE'
  shaderLightingPass.setFloat("far_plane", m_far);
  shaderLightingPass.setVec3("viewPos", camera.Position);
  //TODO: Make Graphic Manager and move.
  renderQuad();
  if(glGetError() != GL_NO_ERROR)
    throw("");
  
}
void DeferredPointShadow::ReleaseBuffer(void)
{
  glBindFramebuffer(GL_READ_FRAMEBUFFER, s_framebuffer[!s_num]);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // write to default framebuffer
  // blit to default framebuffer. Note that this may or may not work as the internal formats of both the FBO and default framebuffer have to match.
  // the internal formats are implementation defined. This works on all of my systems, but if it doesn't on yours you'll likely have to write to the
  // depth buffer in another shader stage (or somehow see to match the default framebuffer's internal format with the FBO's internal format).
  glBlitFramebuffer(0, 0, SCR_WIDTH, SCR_HEIGHT, 0, 0, SCR_WIDTH, SCR_HEIGHT, GL_COLOR_BUFFER_BIT, GL_NEAREST);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredPointShadow::SetPosition(const glm::vec3& pos)
{ 
  m_pos = pos;
}

glm::vec3 DeferredPointShadow::GetPosition(void)
{
  return m_pos;
}

void DeferredPointShadow::SetLightPower(const LightConstant& power)
{ 
  m_power = power;
}

LightConstant DeferredPointShadow::GetLightPower(void)
{
  return m_power;
}


void DeferredPointShadow::TEMP_DrawFloorOfDepth(const glm::mat4 & MVP)
{
  Shader DepthShader{s_depthShaderID};
  DepthShader.use();
  DepthShader.setMat4("model", MVP);
  glViewport(0, 0, m_mapSize.x, m_mapSize.y);
  glBindFramebuffer(GL_FRAMEBUFFER, m_depthMapFBO);
  unsigned diff, spec;
  TEMP_GetFloorTexture(diff, spec);
  renderFloor(diff, spec);

  if(glGetError() != GL_NO_ERROR)
    throw("");
  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

}

void DeferredPointShadow::DEBUG_DrawLightBox(const glm::mat4& projection, const glm::mat4& view)
{
#ifdef DEBUG
  // 3. render lights on top of scene
  // --------------------------------
  Shader shaderLightBox{GetCubeShader()};
  shaderLightBox.use();
  shaderLightBox.setMat4("projection", projection);
  shaderLightBox.setMat4("view", view);
  glm::mat4 model{1.0};
  model = glm::translate(model, glm::vec3{m_pos});
  model = glm::scale(model, glm::vec3(0.125f));
  shaderLightBox.setMat4("model", model);
  shaderLightBox.setVec3("lightColor", m_color);
  renderCube();
#endif
#ifdef _DEBUG
  // 3. render lights on top of scene
  // --------------------------------
  Shader shaderLightBox{GetCubeShader()};
  shaderLightBox.use();
  if(glGetError() != GL_NO_ERROR)
    throw("");
  shaderLightBox.setMat4("projection", projection);
  shaderLightBox.setMat4("view", view);
  glm::mat4 model{1.0};
  model = glm::translate(model, glm::vec3{m_pos});
  model = glm::scale(model, glm::vec3(0.03f));
  shaderLightBox.setMat4("model", model);
  shaderLightBox.setVec3("lightColor", m_color);
  renderCube();
#endif
  return;
}


void DeferredPointShadow::destroy(void)
{
  glDeleteFramebuffers(1, &m_depthMapFBO);
  glDeleteTextures(1, &m_depthCubeMap);
  //glDeleteVertexArrays();
  
  return;
}

void DeferredPointShadow::SetCubemapID(unsigned id)
{
  s_textureId = id;
}


void DeferredPointShadow::SetDepthMapShaderID(unsigned id)
{
  s_depthShaderID = id;
}

void DeferredPointShadow::SetRenderShaderID(unsigned id, const gBuffer& gBufferid)
{
  s_shaderId = id;
  gbuf = gBufferid;
}

void DeferredPointShadow::InitFrambuffer(void)
{ 
  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

  s_num = false;
  glGenFramebuffers(2, s_framebuffer.data());
  glGenTextures(2, s_textureColorBuffer.data());

  glBindFramebuffer(GL_FRAMEBUFFER, s_framebuffer[0]);
  glBindTexture(GL_TEXTURE_2D, s_textureColorBuffer[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, s_textureColorBuffer[0], 0);

  glBindFramebuffer(GL_FRAMEBUFFER, s_framebuffer[1]);
  glBindTexture(GL_TEXTURE_2D, s_textureColorBuffer[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, s_textureColorBuffer[1], 0);
}

std::array<const unsigned, 2> DeferredPointShadow::GetFrameBuffer(void)
{
  return {s_framebuffer[0], s_framebuffer[1]};
}

std::array<const unsigned, 2> DeferredPointShadow::GetTextureColorBuffer(void)
{
  return {s_textureColorBuffer[0], s_textureColorBuffer[1]};
}

bool DeferredPointShadow::GetFrameNumber(void)
{
  return s_num;
}
