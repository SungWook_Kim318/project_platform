// GL Libs
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Basic Libs
#include <iostream>
#include <limits>
#include <list>

#include <vector>
#include <random>
#include <cmath>

// Custom Libs
#include "Shader.hpp"
#include "Camera.hpp"
#include "Model.hpp"
#include "Graphics.hpp"
#include "light.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "../extern/stb_image.h"

void windowSize_CB(GLFWwindow* window, int width, int height);
void Keyboard_CB(GLFWwindow *window);
void Mouse_CB(GLFWwindow* window, double xpos, double ypos);
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset);
unsigned loadTexture(const char *path, bool gammaCorrection);

// settings
unsigned int SCR_WIDTH = 1280;
unsigned int SCR_HEIGHT = 720;

// camera
Camera camera(glm::vec3{0.0f, 10.0f, 15.0f}, glm::vec3{0.0f, 1.0f, 0.0f}, -90, -45);
float lastX = SCR_WIDTH / 2.0;
float lastY = SCR_HEIGHT / 2.0;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;  // time between current frame and last frame
float lastFrame = 0.0f;

// Light Variable
static float linear = 0.5;
static float quadratic = 0.5;

static glm::vec4 ambientPower;
std::vector<DeferredPointShadow> lights{};

static inline
float lerp(float a, float b, float f)
{
  return a + f * (b - a);
}

int main(int argc, char** argv)
{
  //--------------------------- INIT PART ---------------------------
  bool res = glfwInit();
  if(!res)
  {
    std::cout << "ERROR! glfw init\n";
    return -1;
  }
  //macOS setting
  //OpenGL 4.1
  //glfwWindowHint(GLFW_SAMPLES, 8);

#ifdef __APPLE__
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#else
  //Other setting
  //OpenGL 4.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
#endif

  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


  //Create Process and Window
  GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
  if(window == NULL)
  {
    //TODO : change throwing exception.
    //throw()
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }

  //Add Callback function to change Window Size
  //Make Make GL Context
  glfwMakeContextCurrent(window);

  // tell GLFW to capture our mouse
  glfwSetFramebufferSizeCallback(window, windowSize_CB);
  glfwSetCursorPosCallback(window, Mouse_CB);
  glfwSetScrollCallback(window, MouseScroll_CB);

  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  //Init glew
  glewExperimental = true; // Needed for core profile
  if(glewInit() != GLEW_OK)
  {
    fprintf(stderr, "%d: Failed to initialize GLEW\n", __LINE__);
    getchar();
    glfwTerminate();
    return -1;
  }

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);

  // Shaders
  Shader shaderGeometryPass{"9.ssao_geometry.vs", "9.ssao_geometry.fs"};
  Shader shaderLightingPass{"9.ssao.vs", "9.ssao_lighting.fs"};
  Shader shaderSSAO{"9.ssao.vs", "9.ssao.fs"};
  Shader shaderSSAOBlur{"9.ssao.vs", "9.ssao_blur.fs"};

  Shader debugDepthQuad{"debug/debug_depth.vs","debug/debug_depth.fs"};
  Shader debugBufferQuad{"debug/debug_gbuffer.vs", "debug/debug_gbuffer.fs"};
  
  // Load model
  Model nanosuit{"nanosuit/nanosuit.obj"};

  // configure g-buffer framebuffer
  // ------------------------------
  unsigned int gBuffer;
  glGenFramebuffers(1, &gBuffer);
  glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
  unsigned int gPosition, gNormal, gAlbedo;
  // position color buffer
  glGenTextures(1, &gPosition);
  glBindTexture(GL_TEXTURE_2D, gPosition);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0);
  // normal color buffer
  glGenTextures(1, &gNormal);
  glBindTexture(GL_TEXTURE_2D, gNormal);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);
  // color + specular color buffer
  glGenTextures(1, &gAlbedo);
  glBindTexture(GL_TEXTURE_2D, gAlbedo);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo, 0);
  // tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
  unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
  glDrawBuffers(3, attachments);
  // create and attach depth buffer (renderbuffer)
  unsigned int rboDepth;
  glGenRenderbuffers(1, &rboDepth);
  glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCR_WIDTH, SCR_HEIGHT);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
  // finally check if framebuffer is complete
  if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "Framebuffer not complete!" << std::endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  
  // also create framebuffer to hold SSAO processing stage 
  // -----------------------------------------------------
  unsigned ssaoFBO, ssaoBlurFBO;
  glGenFramebuffers(1, &ssaoFBO);
  glGenFramebuffers(1, &ssaoBlurFBO);
  glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
  unsigned ssaoColorBuffer, ssaoColorBufferBlur;
  // SSAO color buffer
  glGenTextures(1, &ssaoColorBuffer);
  glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, 
               SCR_WIDTH, SCR_HEIGHT, 0,
               GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer, 0);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "SSAO Framebuffer not complete!" << std::endl;

  // and blur stage
  glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
  glGenTextures(1, &ssaoColorBufferBlur);
  glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    std::cout << "SSAO Blur Framebuffer not complete!" << std::endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  // generate sample kernel
  // ----------------------
  std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
  std::default_random_engine generator;
  std::vector<glm::vec3> ssaoKernel;
  ssaoKernel.reserve(64);
  for(unsigned int i = 0; i < 64; ++i)
  {
    glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
    sample = glm::normalize(sample);
    sample *= randomFloats(generator);
    float scale = float(i) / 64.0;

    // scale samples s.t. they're more aligned to center of kernel
    scale = lerp(0.1f, 1.0f, scale * scale);
    sample *= scale;
    ssaoKernel.push_back(sample);
  }

  // generate noise texture
  // ----------------------
  std::vector<glm::vec3> ssaoNoise;
  ssaoNoise.reserve(16);
  for(unsigned int i = 0; i < 16; i++)
  {
    glm::vec3 noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
    ssaoNoise.push_back(noise);
  }
  unsigned int noiseTexture; glGenTextures(1, &noiseTexture);
  glBindTexture(GL_TEXTURE_2D, noiseTexture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  // lighting info
  // -------------
  glm::vec3 lightPositions;
  glm::vec3 lightColors;
  glm::vec3 lightVelo;
  constexpr float lightConstant = 0.1f;
  srand(13);
  
  lightPositions = glm::vec3{1.0,1.0,1.0};
  lightVelo = glm::vec3{0};
  lightColors = glm::vec3{0.8,1.0,1.0};
  
  // shader configuration
  // --------------------
  shaderLightingPass.use();
  shaderLightingPass.setInt("gPosition", 0);
  shaderLightingPass.setInt("gNormal", 1);
  shaderLightingPass.setInt("gAlbedo", 2);
  shaderLightingPass.setInt("ssao", 3);
  shaderSSAO.use();
  shaderSSAO.setInt("gPosition", 0);
  shaderSSAO.setInt("gNormal", 1);
  shaderSSAO.setInt("texNoise", 2);
  shaderSSAOBlur.use();
  shaderSSAOBlur.setInt("ssaoInput", 0);

  // lighting info
  // -------------
  glm::vec3 lightPos = glm::vec3(2.0, 4.0, -2.0);
  glm::vec3 lightColor = glm::vec3(0.2, 0.2, 0.7);

  // DEBUG
  debugDepthQuad.use();
  debugDepthQuad.setInt("depthMap", 0);
  
  // --------------------------- UPDATE PART ---------------------------
  glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
  DeferredPointShadow::InitFrambuffer();
  std::array<const unsigned, 2> c_framebuffer = DeferredPointShadow::GetFrameBuffer();
  std::array<const unsigned, 2> c_textureColor = DeferredPointShadow::GetTextureColorBuffer();

  //TODO: Game Update
  do {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    // --------- handling input
    Keyboard_CB(window);
    
    // Physics Part
    // ----------------------------------------------------------------------
    /*
    static glm::vec3 add{1, 0.0, 1};
    if(objectPositions[0].x <= -3.0)
    {
      add *= -1;
      objectPositions[0] = glm::vec3{-3.0, -3.0, -3.0} + add * deltaTime;
    }
    else if(objectPositions[0].x >= 3.0)
    {
      add *= -1;
      objectPositions[0] = glm::vec3{3.0, -3.0, 3.0} + add * deltaTime;
    }
    else
      objectPositions[0] += add * deltaTime;
    for(auto& light : lights)
    {
      light.update();
    }
    */
    // ----------------------------------------------------------------------
    
    // render
    // ------
    // Clear buffers
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    

    // 1. geometry pass: render scene's geometry/color data into gbuffer
    // -----------------------------------------------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 50.0f);
    glm::mat4 view = camera.GetViewMatrix();
    glm::mat4 model = glm::mat4(1.0f);
    shaderGeometryPass.use();
    shaderGeometryPass.setMat4("projection", projection);
    shaderGeometryPass.setMat4("view", view);
    // room cube
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0, 7.0f, 0.0f));
    model = glm::scale(model, glm::vec3(7.5f, 7.5f, 7.5f));
    shaderGeometryPass.setMat4("model", model);
    shaderGeometryPass.setInt("invertedNormals", 1); // invert normals as we're inside the cube
    renderCube();
    shaderGeometryPass.setInt("invertedNormals", 0);
    // nanosuit model on the floor
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, 5.0));
    model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0, 0.0, 0.0));
    model = glm::scale(model, glm::vec3(0.5f));
    shaderGeometryPass.setMat4("model", model);
    nanosuit.Draw(shaderGeometryPass);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 2. generate SSAO texture
    // ------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
    glClear(GL_COLOR_BUFFER_BIT);
    shaderSSAO.use();
    // Send kernel + rotation 
    for(unsigned int i = 0; i < 64; ++i)
      shaderSSAO.setVec3("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
    shaderSSAO.setMat4("projection", projection);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    renderQuad();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 3. blur SSAO texture to remove noise
    // ------------------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
    glClear(GL_COLOR_BUFFER_BIT);
    shaderSSAOBlur.use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
    renderQuad();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 4. lighting pass: traditional deferred Blinn-Phong lighting with added screen-space ambient occlusion
    // -----------------------------------------------------------------------------------------------------
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    shaderLightingPass.use();
    // send light relevant uniforms
    glm::vec3 lightPosView = glm::vec3(camera.GetViewMatrix() * glm::vec4(lightPos, 1.0));
    shaderLightingPass.setVec3("light.Position", lightPosView);
    shaderLightingPass.setVec3("light.Color", lightColor);
    // Update attenuation parameters
    const float constant = 1.0; // note that we don't send this to the shader, we assume it is always 1.0 (in our case)
    const float linear = 0.09;
    const float quadratic = 0.032;
    shaderLightingPass.setFloat("light.Linear", linear);
    shaderLightingPass.setFloat("light.Quadratic", quadratic);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gAlbedo);
    glActiveTexture(GL_TEXTURE3); // add extra SSAO texture to lighting pass
    glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
    glViewport(0, SCR_HEIGHT / 8, SCR_WIDTH, SCR_HEIGHT * 7 / 8);
    renderQuad();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    // Render G buffer map to quad for visual debugginf
    debugBufferQuad.use();
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosition);
    glViewport(0, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    glViewport(SCR_WIDTH / 8, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();
    
    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gAlbedo);
    glViewport(SCR_WIDTH / 8 * 2, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();

    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
    glViewport(SCR_WIDTH / 8 * 3, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();

    debugBufferQuad.setMat4("tranformation", glm::mat4{1.0f});
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
    glViewport(SCR_WIDTH / 8 * 4, 0, SCR_WIDTH / 8, SCR_HEIGHT / 8);
    renderQuad();

    glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
    // -------- glfw Update
    glfwSwapBuffers(window);
    glfwPollEvents();
    
  } while (!glfwWindowShouldClose(window));
  
  // ---------------------------- END PART ----------------------------
  // glfw: terminate, clearing all previously allocated GLFW resources.
  // ------------------------------------------------------------------
  glfwTerminate();
  
  return 0;
}

// Callback Function
void windowSize_CB(GLFWwindow* window, int width, int height)
{
  SCR_WIDTH = width;
  SCR_HEIGHT = height;
  glViewport(0, 0, width, height);
}
//TODO: Move Input manager.
void Keyboard_CB(GLFWwindow *window)
{
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera.ProcessKeyboard(FORWARD, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera.ProcessKeyboard(BACKWARD, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera.ProcessKeyboard(LEFT, deltaTime);
  if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera.ProcessKeyboard(RIGHT, deltaTime);

  //change Light Power
  if(glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.linear += 0.01f;
    std::cout << "linear : " << lc.linear.r << ',' << lc.linear.g << ',' << lc.linear.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
  else if(glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.linear.r <= 0.0001f ? lc.linear.r = 0.0001f : lc.linear.r -= 0.01f;
    lc.linear.g <= 0.0001f ? lc.linear.g = 0.0001f : lc.linear.g -= 0.01f;
    lc.linear.b <= 0.0001f ? lc.linear.b = 0.0001f : lc.linear.b -= 0.01f;
    std::cout << "linear : " << lc.linear.r << ',' << lc.linear.g << ',' << lc.linear.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
  if(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.diffuse += 0.01f;
    std::cout << "diffuse : " << lc.diffuse.r << ',' << lc.diffuse.g << ',' << lc.diffuse.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
  else if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.diffuse.r <= 0.0001f ? lc.diffuse.r = 0.0001f : lc.diffuse.r -= 0.01f;
    lc.diffuse.g <= 0.0001f ? lc.diffuse.g = 0.0001f : lc.diffuse.g -= 0.01f;
    lc.diffuse.b <= 0.0001f ? lc.diffuse.b = 0.0001f : lc.diffuse.b -= 0.01f;
    std::cout << "diffuse : " << lc.diffuse.r << ',' << lc.diffuse.g << ',' << lc.diffuse.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
  if(glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS)
  {
    if(lights.size() > 2)
    {
      lights[lights.size() - 1].destroy();
      lights.pop_back();
    }
    std::cout << "Light number : " << lights.size() << '\n';
  }
  if(glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
  {
    if(lights.size() < 30)
    {
      for(auto& light : lights)
        light.destroy();

      const unsigned index = lights.size();
      lights.push_back(DeferredPointShadow{ });
      auto& random =
        [](float min, float max)  -> float
      {
        return min + static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX) * (max - min);
      };
      lights[index].SetPosition(glm::vec3{random(-9.0,9.0),random(1.0,3.0),random(-9.0,9.0)});
      lights[index].SetLightPower(lights[0].GetLightPower());

      for(auto& light : lights)
        light.init();
    }
    
    
    std::cout << "Light number : " << lights.size() << '\n';
  }
  if(glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.quadratic += 0.01f;
    std::cout << "Quardratic : " << lc.quadratic.r << ',' << lc.quadratic.g << ',' << lc.quadratic.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
  else if(glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
  {
    LightConstant lc = lights[0].GetLightPower();
    lc.quadratic.r <= 0.0001f ? lc.quadratic.r = 0.0001f : lc.quadratic.r -= 0.01f;
    lc.quadratic.g <= 0.0001f ? lc.quadratic.g = 0.0001f : lc.quadratic.g -= 0.01f;
    lc.quadratic.b <= 0.0001f ? lc.quadratic.b = 0.0001f : lc.quadratic.b -= 0.01f;
    std::cout << "Quardratic : " << lc.quadratic.r << ',' << lc.quadratic.g << ',' << lc.quadratic.b << '\n';
    for(auto& light : lights)
    {
      light.SetLightPower(lc);
    }
  }
}
// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void Mouse_CB(GLFWwindow* window, double xpos, double ypos)
{
  if(firstMouse)
  {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

  lastX = xpos;
  lastY = ypos;

  camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void MouseScroll_CB(GLFWwindow* window, double xoffset, double yoffset)
{
  camera.ProcessMouseScroll(yoffset);
}


// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned loadTexture(const char *path, bool gammaCorrection)
{
  unsigned int textureID;
  glGenTextures(1, &textureID);
  
  int width, height, nrComponents;
  unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
  if (data)
  {
    GLenum internalFormat = 0;
    GLenum dataFormat = 0;
    if (nrComponents == 1)
    {
      internalFormat = dataFormat = GL_RED;
    }
    else if (nrComponents == 3)
    {
      internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
      dataFormat = GL_RGB;
    }
    else if (nrComponents == 4)
    {
      internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
      dataFormat = GL_RGBA;
    }
    
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    stbi_image_free(data);
  }
  else
  {
    std::cout << "Texture failed to load at path: " << path << std::endl;
    stbi_image_free(data);
  }
  
  return textureID;
}
