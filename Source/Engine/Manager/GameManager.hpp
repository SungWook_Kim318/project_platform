//
//  GameManager.hpp
//  playground
//
//  Created by kmu-lab on 11/07/2018.
//

#pragma once

class GameManager {
private:
  static GameManager* psGM;
  
  GameManager(void);
  GameManager(const GameManager&) = delete;
  const GameManager& operator=(const GameManager&) = delete;
public:
  GameManager& operator() (void);
  static GameManager& instance(void);
  
  void Init(void);
  void OnStart(void);
  void OnUpdate(void);
  void OnClose(void);
  void Destroy(void);
};
