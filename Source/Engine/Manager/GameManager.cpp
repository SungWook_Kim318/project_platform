//
//  GameManager.cpp
//  playground
//
//  Created by kmu-lab on 11/07/2018.
//

#include "GameManager.hpp"

#include <GLFW/glfw3.h>


GameManager* GameManager::psGM = new GameManager();

/*
 static GameManager* psGM;
 
 GameManager(void);
 GameManager(const GameManager&) = delete;
 
 public:
 GameManager& operator() (void);
 static GameManager& instance(void);
 
 void Init(void);
 */

GameManager::GameManager(void)
//:
{
  
}
GameManager& GameManager::operator()(void)
{
  return *psGM;
}
GameManager& GameManager::instance(void)
{
  return *psGM;
}
void GameManager::Init(void)
{
  
}
void GameManager::OnStart(void)
{
  
}
void GameManager::OnUpdate(void)
{
  
}
void GameManager::OnClose(void)
{
  
}
void GameManager::Destroy(void)
{
  
}
