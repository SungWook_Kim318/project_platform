//
//  light.hpp
//  playground
//
//  Created by kmu-lab on 17/10/2018.
//
#pragma once

#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <array>

#include "Model.hpp"

struct LightConstant
{
  glm::vec3 diffuse;
  glm::vec3 linear;
  glm::vec3 quadratic;
  constexpr LightConstant(void)
  : diffuse(glm::vec3{1.0f}), linear(glm::vec3{0.10f}), quadratic(glm::vec3{0.10f})
  {}
  LightConstant(float diff, float line, float quad)
  : diffuse(glm::vec3{diff}), linear(glm::vec3{line}), quadratic(glm::vec3{quad})
  {}
  LightConstant(glm::vec3 diff, glm::vec3 line, glm::vec3 quad)
  : diffuse(diff), linear(line), quadratic(quad)
  {}
};

struct gBuffer
{
  unsigned gWorldPos;
  unsigned gNormal;
  unsigned gAlbedo;
};

class DeferredPointShadow
{
private:
  glm::vec3 m_pos;
  glm::vec3 m_color;
  LightConstant m_power;
  
  float m_rad;
  float m_near;
  float m_far;
  
  std::array<glm::mat4, 6> m_shadowMtx;
  
  const glm::uvec2 m_mapSize;
  unsigned m_depthMapFBO;
  unsigned m_depthCubeMap;
  
  static unsigned s_textureId;
  static unsigned s_shaderId;
  static unsigned s_depthShaderID;

  static std::array<unsigned, 2> s_framebuffer;
  static std::array<unsigned, 2> s_textureColorBuffer;
  static bool s_num;

  static gBuffer gbuf;
  bool initailed;

  
public:
  static constexpr glm::uvec2 size_2048{2048};
  static constexpr glm::uvec2 size_1024{1024};
  static constexpr glm::uvec2 size_512{512};
  
  constexpr DeferredPointShadow(void)
  : m_pos(), m_color(glm::vec3{1.0}), m_power(),
  m_rad(1.570796f), m_near(0.0001f), m_far(25.0), m_shadowMtx(), m_mapSize(size_1024), m_depthMapFBO(), m_depthCubeMap(), initailed(false)
  {}
  DeferredPointShadow(const DeferredPointShadow&);

  ~DeferredPointShadow(void);
  bool init(void);
  bool update(void);
  void destroy(void);

  void DrawDepthMap(Model& object, const glm::mat4& MVP);
  void Draw(void);

  void SetPosition(const glm::vec3& pos);
  glm::vec3 GetPosition(void);

  void SetLightPower(const LightConstant& power);
  LightConstant GetLightPower(void);


  void TEMP_DrawFloorOfDepth(const glm::mat4& MVP);
  void DEBUG_DrawLightBox(const glm::mat4& projection, const glm::mat4& view);

  void DeleteLight(void);
  // Graphic's Manager Stuff
  //! Set Cubemap Texture ID
  static void SetCubemapID(unsigned id);
  //! Set DepthCubeMap Shader ID
  static void SetDepthMapShaderID(unsigned id);
  //! Set Rendering Shader ID
  static void SetRenderShaderID(unsigned id, const gBuffer& gBufferid);

  static void InitFrambuffer(void);
  static std::array<const unsigned, 2> GetFrameBuffer(void);
  static std::array<const unsigned, 2> GetTextureColorBuffer(void);
  static bool GetFrameNumber(void);
  
  static void ReleaseBuffer(void);

};
