#pragma once
void SetCubeShader(unsigned id);
unsigned GetCubeShader(void);

void TEMP_SetFloorTexture(unsigned diff, unsigned spec);
void TEMP_GetFloorTexture(unsigned& diff, unsigned& spec);

void renderCube();
void renderQuad();
void renderFloor(const unsigned diffuse, const int specular = -1);