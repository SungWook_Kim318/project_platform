#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D shadowMap;

struct Light {
  vec3 Position;
  vec3 Color;
  
  float Linear;
  float Quadratic;
  float Radius;
  
  mat4 LightsMtx;
};

const int NR_LIGHTS = 1;
uniform Light lights[NR_LIGHTS];
uniform vec3 viewPos;

const float constantK = 0.1f;

float ShadowCalculation(vec4 fragPosLightSpace)
{
  // perform perspective divide
  vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
  // transform to [0,1] range
  //projCoords = projCoords * 0.5 + 0.5;
  // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
  float closestDepth = texture(shadowMap, projCoords.xy).r;
  // get depth of current fragment from light's perspective
  float currentDepth = projCoords.z;
  // check whether current frag pos is in shadow
  float shadow = currentDepth > closestDepth  ? 1.0 : 0.0;
  
  return shadow;
}

void main()
{
  // retrieve data from gbuffer
  vec3 FragPos = texture(gPosition, TexCoords).rgb;
  vec3 Normal = texture(gNormal, TexCoords).rgb;
  vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
  float Specular = texture(gAlbedoSpec, TexCoords).a;
  
  // then calculate lighting as usual
  vec3 lighting  = Diffuse * 0.1; // hard-coded ambient component
  vec3 viewDir  = normalize(viewPos - FragPos);
  for(int i = 0; i < NR_LIGHTS; ++i)
  {
    float dist = length(lights[i].Position - FragPos);
    if(dist < lights[i].Radius)
    {
      // diffuse
      vec3 lightDir = normalize(lights[i].Position - FragPos);
      vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lights[i].Color;
      // specular
      vec3 halfwayDir = normalize(lightDir + viewDir);
      float spec = pow(max(dot(Normal, halfwayDir), 0.0), 16.0);
      vec3 specular = lights[i].Color * spec * Specular;
      // attenuation
      float distance = length(lights[i].Position - FragPos);
      float attenuation = 1.0 / (constantK + lights[i].Linear * distance + lights[i].Quadratic * distance * distance);
      diffuse *= attenuation;
      specular *= attenuation;
      // Shadow
      //FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
      vec4 FragPosLightSpace = vec4(FragPos, 1.0);
      FragPosLightSpace *= lights[i].LightsMtx;
      float visibility = ShadowCalculation(FragPosLightSpace);
      visibility = 1.0 - visibility;
      // Result
      lighting += visibility * ( diffuse + specular );
      //lighting = vec3(visibility);
    }
  }
  FragColor = vec4(lighting, 1.0);
}
