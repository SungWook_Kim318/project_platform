#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D frambuffer;

uniform mat4 tranformation;


void main()
{
  vec4 pixel = texture(frambuffer, TexCoords).rgba;
 
  FragColor = tranformation * pixel; // orthographic
}
