#version 330 core

in vec2 TexCoords;
out vec4 outColor;
uniform sampler2D gPosition; //Ok
uniform sampler2D gNormal; //Ok
uniform sampler2D gAlbedoSpec; //Ok
uniform samplerCube depthMap; //Ok
uniform sampler2D backBuffer;//OK

const int NR_LIGHTS = 1;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform float lightConstant;
uniform float lightLinear;
uniform float lightQuadratic;


uniform vec3 viewPos; //CamPosition

uniform float far_plane;
uniform bool shadows = true;

// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
 vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1),
 vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
 vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
 vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
 vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
 );


float ShadowCalculation(vec3 fragPos)
{
  
  vec3 fragToLight = fragPos - lightPos;

  float currentDepth = length(fragToLight);

  float shadow = 0.0;
  float bias = 0.15;
  int samples = 20;
  float viewDistance = length(viewPos - fragPos);
  float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;
  
  for(int i = 0; i < samples; ++i)
  {
    float closestDepth = texture(depthMap, fragToLight + gridSamplingDisk[i] * diskRadius).r;
    closestDepth *= far_plane;   // undo mapping [0;1]
    if(currentDepth - bias > closestDepth)
      shadow += 1.0;
  }
  shadow /= float(samples);

  return shadow;
}


void main()
{
  // retrieve data from gbuffer
  vec3 FragPos = texture(gPosition, TexCoords).rgb;
  vec3 Normal = texture(gNormal, TexCoords).rgb;
  vec3 Diffuse = texture(gAlbedoSpec, TexCoords).rgb;
  float Specular = texture(gAlbedoSpec, TexCoords).a;
  vec3 backColor = texture(backBuffer, TexCoords).rgb;
  // then calculate lighting as usual
  vec3 lighting  = Diffuse * 0.3; // hard-coded ambient component
  vec3 viewDir  = normalize(viewPos - FragPos);

  float dist = length(lightPos - FragPos);
  // diffuse
  vec3 lightDir = normalize(lightPos - FragPos);
  vec3 diffuse = max(dot(Normal, lightDir), 0.0) * Diffuse * lightColor;
  // specular
  vec3 halfwayDir = normalize(lightDir + viewDir);
  float spec = pow(max(dot(Normal, halfwayDir), 0.0), 64.0);
  vec3 specular = lightColor * spec * Specular;
  // attenuation
  float distance = length(lightPos - FragPos);
  float attenuation = 1.0 / (lightConstant + lightLinear * distance + lightQuadratic * distance * distance);
  diffuse *= attenuation;
  specular *= attenuation;

  float shadow = shadows ? ShadowCalculation(FragPos) : 0.0;

  // Result
  lighting += (1.0 - shadow) * ( diffuse + specular );
  //lighting = vec3(1.0 - shadow);

 // gl_FragColor += vec4(lighting, 1.0);
  //gl_FragColor = mix(gl_FragColor, vec4(lighting, 1.0), 0.5);
  outColor = vec4(backColor + lighting, 1.0);
}
